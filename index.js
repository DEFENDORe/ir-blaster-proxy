const express = require('express')
//const request = require('request')
var execSync = require('child_process').execSync

var videoURL = "http://localhost:8080/"
var irExecutable = "echo"
var port = 8181

const app = express()

app.get('/', (req, res) => {
    res.send('IR Blaster / HTTP Proxy')
})

app.get('/video', function(req, res) {
  var channel = req.query.channel
  var cmd = irExecutable + " " + channel
  console.log("Channel: " + channel + " - Executing CMD: " + cmd)
  execSync(cmd)
  //request(videoURL).pipe(res)
  res.redirect(videoURL)
})

app.listen(port, () => {
    console.log("IR Blaster / HTTP Proxy running on port: " + port)
})